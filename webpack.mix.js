const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// import { NativeEventSource, EventSourcePolyfill } from 'event-source-polyfill';
 
// const EventSource = NativeEventSource || EventSourcePolyfill;
// // OR: may also need to set as global property
// global.EventSource =  NativeEventSource || EventSourcePolyfill;

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
