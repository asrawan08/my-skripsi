<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Localization
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});


Route::get('/', 'AppController@index');
Route::get('/mobodrive', 'AppController@driver');
Route::get('/shipmentdetail', 'AppController@order');
Route::get('/managementnotif', 'AppController@notif');
Route::get('/notificationmessage', 'AppController@notificationMessage');
Route::get('/viewdetailnotification', 'AppController@viewdetailnotification');
Route::get('/verifyactivity', function(){
    return view('VerifyActivity');
});
Route::get('/login', function (){
    Artisan::call('cache:clear');
    return view('auth/login');
});
// Route::get('/driver', function () {
//     return view('driver');
// });

Route::post('/proses', 'AppController@proses');

// Route::get('/messages', function () {
//     return view('messages');
// });
Route::get('/logout', 'AppController@logout');
// Auth::routes();

Route::get('/home', 'AppController@home');

// Route::post('/test/{eventUpload}', function () {
//     return view('popupExternal');
// });
Route::get('/popupUpload', 'AppController@popupUpload');

Route::get('/testevents', function () {
    return view('events');
});
Route::get('/testmaps', function () {
    return view('liveTracking');
});
Route::get('/eventmanagement', function () {
    return view('eventmanagement');
});

Route::get('/dashboard', 'AppController@dashboard');
// Route::get('/dashboard', function () {
//     return view('dashboard');
// });


// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
Route::get('/inbox', 'AppController@inbox')->name('inbox');
Route::get('/order', 'AppController@order')->name('order');
Route::get('/mobomap', 'AppController@shipmentLocation')->name('shipmentLocation');

Route::get('/testmodal', function () {
    return view('modal');
});

Route::get('/instance_checkpoint_log', 'AppController@monitoringlog');

//Administration
Route::get('/administration','AppController@administration');
Route::post('/administration/saveuser','AppController@saveuser');
Route::post('/administration/savewindow','AppController@savewindow');
Route::post('/administration/updateuser','AppController@updateuser');
Route::post('/administration/updatewindow','AppController@updateWindow');
Route::post('/administration/reloadusers','AppController@reloadusers');
Route::delete('/administration/deleteuser','AppController@deleteuser');
Route::delete('/administration/deletewindow','AppController@deleteWindow');
Route::delete('/administration/deletepermission','AppController@deletePermission');
Route::delete('/administration/killsession','AppController@killsession');
Route::post('/administration/updateprofile','AppController@updateprofile');

//Setting
Route::get('/setting','AppController@setting');
Route::post('/setting/saverole','AppController@saverole');
Route::post('/setting/updaterole','AppController@updaterole');
Route::get('/setting/deleterole/{role_id}','AppController@deleterole');
Route::post('/setting/saveadmin','AppController@saveadmin');
Route::post('/setting/updateadmin','AppController@updateadmin');
Route::get('/setting/deleteadmin/{manageuser_id}','AppController@deleteadmin');

//Refresh token
Route::get('/token','AppController@refreshToken');

//Administration
Route::get('/getparams', 'AppController@getParams');
Route::get('/getprofile', 'AppController@getProfile');
Route::get('/getuserbymember', 'AppController@getUserByMember');
Route::get('/getwindows', 'AppController@getWindows');
Route::get('/getparents', 'AppController@getParents');
Route::get('/getcustomers', 'AppController@getCustomers');

Route::get('/monitoring', 'AppController@moboConfig');

Route::post('fingerprint','AppMyskripsi@fingerPrint');
