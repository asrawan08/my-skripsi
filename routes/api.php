<?php

use Illuminate\Http\Request;
use App\Http\Controllers\AppMyskripsi;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('access/finger-print', 'AppMyskripsi@fingerPrint');
Route::post('access/rfid', 'AppMyskripsi@rfId');
Route::get('access/finger-print', 'AppMyskripsi@getAccessFingerPrint');
Route::get('access/rfid', 'AppMyskripsi@getAccessRfId');
Route::get('access/create-finger-print', 'AppMyskripsi@fingerPrint');
Route::get('access/create-rfid', 'AppMyskripsi@rfId');
Route::get('access/report-rfid', 'AppMyskripsi@getReportAccess');
Route::get('access/in', 'AppMyskripsi@accessIn');
Route::get('access/out', 'AppMyskripsi@accessOut');
Route::get('access/get-total-access', 'AppMyskripsi@getAccountAccess');
Route::get('access/set-total-access', 'AppMyskripsi@setAccountAccess');
Route::get('access/get-parameter', 'AppMyskripsi@getParameter');
Route::post('access/set-message', 'AppMyskripsi@setMessage');
Route::get('access/get-message', 'AppMyskripsi@getMessage');
Route::get('access/get-user', 'AppMyskripsi@getUser');
Route::get('access/get-user-access', 'AppMyskripsi@getReportUserAccess');
