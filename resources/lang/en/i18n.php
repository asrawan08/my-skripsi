<?php
return [
    //Login Page
    'pilihbahasa'       => 'Choose Languages : ',
    'username'          => 'Username',
    'password'          => 'Password',
    'logonto'           => 'Log on To ',
    'login'             => 'LOGIN',
    'forgotpassword'    => 'FORGOT PASSWORD ?',
    'clickhere'         => 'CLICK HERE',
    'contactus'         => 'CONTACT US',
    'version'           => 'Version',
    'copyright'         => 'Copyright 2020 Mobospace.All Rights Reserved',
    'copyrightby'       => 'by Pancaran Group',
    'shipmentdetail'    => 'Shipment Details',
    'livetracking'      => 'LIVE TRACKING',
    'mobodrive'         => 'Mobodrive',
    'welcome'           => 'Hi',
    'notifications'     => 'Notifications',
    'notification'      => 'NOTIFICATION',
    'favorite'          => 'FAVORITE',
    'driver'            => 'Driver',
    'drivergroup'       => 'Driver Group',
    'site'              => 'SITE',
    'attendance'        => 'Attendance',
    'lastlogin'         => 'Last Login',
    'lastloggedin'      => 'Last Logged In',
    'mobostatus'        => 'STATUS MOBO',
    'registered'        => 'Registered',
    'Loggedin'          => 'Logged In',
    'defaultpassword'   => 'Default Password',
    'allowtap'          => 'Allow Tap',
    'addregtap'         => 'Add Register & Tap',
    'addregister'       => 'Add Register',
    'addallowtap'       => 'Add Allow Tap',
    'resetpassword'     => 'Reset Password',
    'resetlogin'        => 'Reset Login',
    'identitas'         => 'ID',
    'phone'             => 'Phone',
    'lastsuccess'       => 'Last Success',
    'at'                => 'At',
    'via'               => 'Via',
    'onsite'            => 'On Site',
    'driverid'          => 'ID Driver',
    'drivername'        => 'Driver Name',
    'address'           => 'Address',
    'lastupdate'        => 'Last Update ',
    'driverlocation'    => 'Driver Location',
    'moboaction'        => 'Action',
    'viewlocationdriver'=> 'Driver Location',
    'refresh'           => 'Refresh',
    'onduty'            => 'On Duty',
    'ready'             => 'Ready',
    'absent'            => 'Absent',
    'filter'            => 'filter',
    'cancel'            => 'CANCEL',
    'savefilter'        => 'SAVE FILTER',
    'save'              => 'SAVE',
    'search'            => 'Search',
    'customer'          => 'Customer',
    'onshipment'          => 'On Shipment',
    'finished'            => 'Finished',
    'ordered'             => 'Ordered',
    'job'                 => 'Job',
    'vehicle&driver'      => 'Vehicle & Driver',
    'shipmentinfo'        => 'shipment Info',
    'progressinfo'        => 'Progress Info',
    'route'               => 'Route',
    'consignee'           => 'Consignee',
    'from'                => 'From',
    'to'                  => 'To',
    'starttime'           => 'Start Time',
    'targetleadtime'      => 'Target Lead Time',
    'elapsedtime'         => 'Elapsed Time',
    'vehicle/driverlocation'  => 'Vehicle/Driver Location',
    'containerno'         => 'No. Container',
    'dataisempty'         => 'DATA IS EMPTY . . .',
    'dashboard'           => 'Dashboard',
    'operationalDashboard'=> 'Operational Dashboard',
    'fleetutilizationkm'    => 'Category',
    'fleetutilizationtrip'    => 'Fleet Utilization - Trip',
    'fleetutilizationdistance'    => 'Fleet Utilization - Distance',
    'periode'             => 'Periode',
    'undefined'           => 'Undefined',
    'unitunavailable'     => 'Unit Unavailable',
    'driversummary'       => 'Driver Summary',
    'other'               => 'Other',
    'availabilityunit'    => 'Availability Unit',
    'ordersummary'        => 'Order Summary',
    'totalunit'           => 'Total Unit',
    'available'           => 'Available',
    'onjob'               => 'On Job',
    'unavailable'         => 'Maintained',
    'billed'              => 'Billed',
    'illegality'          => 'Legality',
    'maintenance'         => 'Maintenance',
    'breakdown'           => 'Breakdown',
    'week'                => 'Week',
    'month'               => 'Month',
    'semester'            => 'Semester',
    'year'                => 'Year',
    'profile'             => 'Profile',
    'usermanagement'      => 'User Management',
    'windowmanagement'    => 'Window Management',
    'usersite'            => 'User Site Internal',
    'administration'      => 'Administration',







];
?>
