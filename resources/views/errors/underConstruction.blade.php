<!DOCTYPE html>
<html>
<head>
    <title>Under Construction</title>
    <meta charset="utf-8">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css')}}">
</head>
<style>
    .link:hover{
    text-decoration: none;
}
</style>
<body>
<div id="app">
    <div align="left" >
        <img src="../assets/images/mobospace.png" style="width: 20%;height: auto; margin-left: 20px; margin-top: 20px">
    </div>
    <div align="center">
        <img src="../assets/images/under-construction.png" style="width: 35%;height: auto;">
        </br>
        <!-- <a href="{{ url('home') }}" >  <button type="button" class="btn btn-primary"> << Back to Home</button></a> -->
        <a class="link" href="{{ url('home') }}" >  
            <v-btn class="text-uppercase font-weight-bold" small rounded style="background-color: #3490dc; color:white;">
                <v-icon>mdi-arrow-left-bold-circle</v-icon> Back to Home
            </v-btn>
        </a>
    </div>
</div>
<script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
