<!DOCTYPE html>
<html>
<head>
    <title>Mobospace | Page Expired</title>
    <link rel="icon" type="image/ico" href="assets/home/favicon.ico" sizes="any"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css')}}">
    <meta http-equiv="refresh" content="4; URL='{{ url('home') }}'" />

</head>
<style>
    .link:hover{
    text-decoration: none;
}
</style>
<body>
<div id="app">
<div align="left" >
        <img src="../assets/images/mobospace.png" style="width: 20%;height: auto; margin-left: 20px; margin-top: 20px">
    </div>
    <div align="center">
        <img src="../assets/images/419.png" style="width: 50%;height: auto;">
        </br>
        <!-- <a href="{{ url('login') }}" >  <button type="button" class="btn btn-primary"> << Back to Home</button></a> -->
        <a class="link" href="{{ url('login') }}" >
            <v-btn class="text-uppercase font-weight-bold" small rounded style="background-color: #3490dc; color:white;">
                <v-icon>mdi-arrow-left-bold-circle</v-icon> Back to Home
            </v-btn>
        </a>
        <!-- <img src="../assets/images/505.png" style="width: 50%;height: auto;">
    </br>
    <a href="{{ url('login') }}" >
        <button class="btn"
                style="
                    background-color: #23a9e1;
                    color: white;
                    font-weight: 900;"
        >
        <i class="mdi mdi-arrow-left-bold-circle"
        aria-hidden="true"></i>
        Back to Home
        </button>
    </a> -->
    </div>
<div>
<script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
