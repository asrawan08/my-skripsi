<!DOCTYPE html>
<html style="overflow:hidden" lang="en">

<head>
  <!-- CSRF Token -->
  <title>Mobospace</title>
  <link rel="icon" type="image/ico" href="assets/home/favicon.ico" sizes="any" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta charset="utf-8">
  <meta http-equiv="Cache-Control" content="no-store"/>
  <!-- Scripts -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css')}}">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <link href='https://fonts.googleapis.com/css?family=Ubuntu:100,300,400,500,700,900|Material+Icons' rel="stylesheet" />
</head>

<body style="background:url(assets/logo/background.jpeg)no-repeat  center fixed; 
  background-size: cover;
  overflow: hidden;">
<!-- <script src="{{ asset('js/hideConsole.js') }}" defer></script> -->
  <!-- background-image: linear-gradient(#A1F6FD, #0b5e7b,#2a1a44) -->
  <div id="app">
    <v-app class="bg-transparent">
      <v-app-bar app :absolute="true" clipped-right elevation="0" color="transparent">
        <v-toolbar-title>
          <a href="{{ url('home') }}">
            <v-img src="assets/logo/Mobospace.png" width="200px" height="auto"></v-img>
          </a>
        </v-toolbar-title>
        <v-flex xs10 md10 text-md-right align-right>
          @if (Session::get('datases'))
          <a style="color: gray;font-size: medium;font-family: 'Ubuntu', sans-serif;font-weight: 600;">
            <a href="{{ url('locale/en') }}" style="color:#23a9e1;font-size:11px;" > <span style="font-weight: 700;">{{ __('i18n.welcome') }} </span>  ,</a> &nbsp; <a style="color: #34a9e0;font-size: medium;font-family: 'Ubuntu', sans-serif;font-weight: 600;">{{Session::get('datases')['nama']}}</a>
          @endif
        </v-flex>
      </v-app-bar>
      <v-content>
        <v-container fill-height align-start py-0 style="max-width:1200px;">
          <v-layout justify-center align-center py-0 my-0 w-100 h-100>
            <!-- <v-flex text-xs-center py-0 my-0 justify-center align-center> -->
            @yield('content')
            <!-- </v-flex> -->
          </v-layout>
        </v-container>
      </v-content>
      <v-footer color="transparent">
        <v-row class="pl-3">
          <menu-footer 
            linkhome="{{ url('/home')}}" 
            linklogout="{{ url('/logout')}}"
            linkadministration="{{ url('/administration')}}"
            linkmoboconfig="{{ url('/monitoring')}}">
          </menu-footer>
        </v-row>
        <!-- <v-row align="end">
          <v-flex text-md-right align-right>
            <messages-footer></messages-footer>
          </v-flex>
            <notification-footer></notification-footer>
        </v-row> -->
      </v-footer>
    </v-app>
  </div>
  @yield('scripts')
  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script> -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzgdorYLt8WgbE5D8kSa8z1qs1hDi1ciA"
  async defer></script> -->

  <!-- <script src="{{ asset('js/ApiUtil.js') }}" defer></script> -->
  <script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>
