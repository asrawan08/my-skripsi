@extends('layout')
    @section('content')
        <administration-table
        usertype="{{Session::get('datases')['logtype']}}"
        vclientid="{{Session::get('datases')['clientid']}}"
        vmemberis="{{Session::get('datases')['memberis']}}"
        customer="{{ __('i18n.customer') }}"
        drivergroup="{{ __('i18n.drivergroup') }}"
        site="{{ __('i18n.site') }}"
        filter="{{ __('i18n.filter') }}"
        cancel="{{ __('i18n.cancel') }}"
        savefilter="{{ __('i18n.savefilter') }}"
        lbl-search="{{ __('i18n.search') }}"
        lbl-profile="{{ __('i18n.profile') }}"
        lbl-usermanagement="{{ __('i18n.usermanagement') }}"
        lbl-windowmanagement="{{ __('i18n.windowmanagement') }}"
        lbl-usersite="{{ __('i18n.usersite') }}"
        lbl-administration="{{ __('i18n.administration') }}"
        >
        </administration-table>
@endsection
