<!DOCTYPE html>
<html>
<head>
  <!-- CSRF Token -->
  <link rel="icon" type="image/ico" href="assets/home/centralnode.ico" sizes="any"/>
    <title>Control Tower</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts --> 
  <link href="css/home.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" type="text/css" rel="stylesheet"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
</head>
<body style="background-image:url({{ asset('xx.jpeg') }})">
  <div id="app">
    <v-app>
    <v-layout row wrap style="max-height: fit-content;">
           <v-flex xs1 id="mTop6" style="margin-left: 60px;">
              <v-img src="assets/home/Cn.png" width="110px"></v-img>
           </v-flex>
           <v-flex xs2 id="mTop6">
            <v-avatar id="width150" style="left: 785px;">
              <a style="color: orange;font-size: medium;">Welcome,</a>
              @if (Session::get('datases'))
              <a id="navbarDropdown" class="nav-link" href="#" role="button" aria-haspopup="true" aria-expanded="false" v-pre style="color: white;font-size: medium;">
              {{Session::get('datases')['username']}}   
              </a>
              @endif
            </v-avatar>                 
           </v-flex>
        </v-layout>
        <header-driver 
        filters='[
          {"title":"On Order", "value": true, "color": "blue dark"},
          {"title":"On Shipment", "value": false, "color": "green"},
          {"title":"Stand By", "color": "white"}
        ]' 
        title="DRIVER"
        buttons='[{"title":"Add Registered"},{"title":"Add Allow Tap"},{"title":"Reset Password"}]'
        ></header-driver>
        <imp-datatables 
                title='[
                    {"title":"On Order", "value": true, "color": "blue dark"},
                    {"title":"On Shipment", "value": false, "color": "green"},
                    {"title":"Stand By", "color": "white"}
                    ]' 
                url="https://api.pancaran-group.co.id/moboadmin/driver/list"
                method="post"
                header='[
                    {"text":"Nama", "value":"name"},
                    {"text":"Id Driver", "value":"driverId"},
                    {"text":"Server Point", "value":"servicePoint"},
                    {"text":"Registrasi", "value":"isPasswordDefault"},
                    {"text":"Logged In", "value":"isLoggedIn"},
                    {"text":"Password Default", "value":"isPasswordDefault"},
                    {"text":"Allow Tap", "value":"isProductionTap"},
                    {"text":"Attedance", "value":"lastLoggedIn"}
                    ]'
                back="DRIVER"
                showselect="true"
                params='{"pageNo":0,"pageSize":10}'
                >
                </imp-datatables>
        <menu-fooster></menu-footer>
        <notification-footer></notification-footer>
        <messages-footer></messages-footer>
    </v-app>
  </div>
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>