<!DOCTYPE html>
<html>
<head>
  <link rel="icon" type="image/ico" href="assets/home/centralnode.ico" sizes="any"/>
  <title>Control Tower</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css')}}">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet" />
</head>

<body>
  <div id="app">
    <v-app>
    <v-layout row wrap style="max-height: fit-content;">
           <v-flex xs1 id="mTop6" style="margin-left: 60px;margin-top: 1%;">
              <a href="{{ url('home') }}">
                <v-img src="assets/home/Cn.png" width="110px"></v-img>
              </a>
           </v-flex>
           <v-flex xs2 id="mTop6" style="margin-top: 1%;">
            <v-avatar id="width150" style="left: 785px;">
              <a style="color: orange;font-size: medium;">Welcome,</a>
              @if (Session::get('datases'))
              <a id="navbarDropdown" class="nav-link" href="#" role="button" aria-haspopup="true" aria-expanded="false" v-pre style="color: white;font-size: medium;">
              {{Session::get('datases')['username']}}
              </a>
              @endif
            </v-avatar>                 
           </v-flex>
        </v-layout>
        <header-inbox 
        title="INBOX"
        ></header-inbox>
    </v-app>
  </div>


  <!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script> -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script> -->
  <script src="{{ asset('js/app.js') }}"></script>
<!--   <script>
    new Vue({
      el: '#app',
      vuetify: new Vuetify(),
    })
  </script> -->
</body>
</html>