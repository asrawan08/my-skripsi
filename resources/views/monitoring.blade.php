@extends('layout')
    @section('content')
        <monitoring
        identitas="{{ __('i18n.identitas') }}"
        phone="{{ __('i18n.phone') }}"
        lastsuccess="{{ __('i18n.lastsuccess') }}"
        at="{{ __('i18n.at') }}"
        via="{{ __('i18n.via') }}"
        onsite="{{ __('i18n.onsite') }}"
        registered="{{ __('i18n.registered') }}"
        Loggedin="{{ __('i18n.Loggedin') }}"
        defaultpassword="{{ __('i18n.defaultpassword') }}"
        allowtap="{{ __('i18n.allowtap') }}"
        driverid="{{ __('i18n.driverid') }}"
        drivername="{{ __('i18n.drivername') }}"
        address="{{ __('i18n.address') }}"
        lastupdate="{{ __('i18n.lastupdate') }}"
        lastloggedin="{{ __('i18n.lastloggedin') }}"
        driverlocation="{{ __('i18n.driverlocation') }}"
        viewlocationdriver="{{ __('i18n.viewlocationdriver') }}"
        refresh="{{ __('i18n.refresh') }}"
        onduty="{{ __('i18n.onduty') }}"
        ready="{{ __('i18n.ready') }}"
        absent="{{ __('i18n.absent') }}"
        moboaction="{{ __('i18n.moboaction') }}"
        driver="{{ __('i18n.driver') }}"
        drivergroup="{{ __('i18n.drivergroup') }}"
        site="{{ __('i18n.site') }}"
        filter="{{ __('i18n.filter') }}"
        cancel="{{ __('i18n.cancel') }}"
        savefilter="{{ __('i18n.savefilter') }}"
        search="{{ __('i18n.search') }}"
        customer="{{ __('i18n.customer') }}"



          buttons='[
            {"title1":"{{ __('i18n.addregtap') }}","status":"/account/add","tapProdaction":"Y"},
            {"title1":"{{ __('i18n.addregister') }}","status":"/account/add","tapProdaction":"N"},
            {"title1":"{{ __('i18n.addallowtap') }}","status":"/account/addTap","tapProdaction":""},
            {"title1":"{{ __('i18n.resetpassword') }}","status":"/account/resetPassword","tapProdaction":""},
            {"title1":"{{ __('i18n.resetlogin') }}","status":"/account/resetLogin","tapProdaction":""}]'
          api-base-url='https://api.pancaran-group.co.id/moboadmin'
          api-data-list='/driver/list'
          api-data-sum='/driver/summary_total'
          method="post"
          header='[
              {"text":"{{ __('i18n.driver') }}", "sortable":false, "value":"name"},
              {"text":"{{ __('i18n.drivergroup') }}","sortable":false, "value":"driverId"},
              {"text":"{{ __('i18n.site') }}", "sortable":false, "value":"servicePoint"},
              {"text":"{{ __('i18n.attendance') }}", "sortable":false, "value":"lastLoggedIn"},
              {"text":"{{ __('i18n.lastlogin') }}", "sortable":false, "value":"isLoggedIn"},
              {"text":"{{ __('i18n.mobostatus') }}", "sortable":false, "value":"isPasswordDefault"}
              ]'
          params='{"pageNo":0,"pageSize":10}'>
        </monitoring>
        @endsection
