<!DOCTYPE html>
<html style="overflow:auto">

<head>
  <!-- CSRF Token -->
  <title>Mobospace</title>
  <link rel="icon" type="image/ico" href="assets/home/favicon.ico" sizes="any" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="https://fonts.googleapis.com/css?family=Exo+2&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/vuetify@1.5.0/dist/vuetify.min.css" rel="stylesheet">
  <link href="{{ asset('css/app.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" type="text/css" rel="stylesheet" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
</head>
<style type="text/css">
  .theme--light.v-label {
    font-size: 12px;
  }

  select.minimal {
    background-image:
      linear-gradient(45deg, transparent 50%, gray 50%),
      linear-gradient(135deg, gray 50%, transparent 50%),
      linear-gradient(to right, #ccc, #ccc);
    background-position:
      calc(100% - 20px) calc(1em + 2px),
      calc(100% - 15px) calc(1em + 2px),
      calc(100% - 2.5em) 0.5em;
    background-size:
      5px 5px,
      5px 5px,
      1px 1.5em;
    background-repeat: no-repeat;
  }

  select.minimal:focus {
    background-image:
      linear-gradient(45deg, blue 50%, transparent 50%),
      linear-gradient(135deg, transparent 50%, blue 50%),
      linear-gradient(to right, #ccc, #ccc);
    background-position:
      calc(100% - 15px) 1em,
      calc(100% - 20px) 1em,
      calc(100% - 2.5em) 0.5em;
    background-size:
      5px 5px,
      5px 5px,
      1px 1.5em;
    background-repeat: no-repeat;
    border-color: blue;
    outline: 0;
  }
</style>

<body style="background:url(assets/logo/background.jpeg)no-repeat  center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  overflow: hidden;">
  <div id="app">
    <v-container grid-list-md text-xs-center>
      <v-layout row wrap style="margin-top: 8%;">
        <v-card class="mx-auto" width="325px" max-width="400px" style="border-radius:5%;">
          <form action="{{URL::to('/proses')}}" method="post">
            @csrf
            <v-card-text class="text--primary text-center">
              <div>
                <v-img src="assets/logo/Mobospace.png" width="240px" id="centerImg" class="ml-4">
                </v-img>
              </div>
              <div>
                <v-text-field class="ml-3 mt-3" label="{{ __('i18n.username') }}" prepend-inner-icon="person" style="font-family: 'Exo 2', sans-serif" id="email" name="email" required>
                </v-text-field>
              </div>
              <div>
                <v-text-field class="ml-3" style="font-family: 'Exo 2', sans-serif;" label="{{ __('i18n.password') }}" prepend-inner-icon="lock" type="password" id="password" name="password" required></v-text-field>
              </div>
              <div class="col-10 px-0 ml-3" style="text-align:left; display:none;" small>
                <span class="ml-2" style="text-align:left;">{{ __('i18n.logonto') }}</span>
                <select id="logtype" name="logtype" class="form-control minimal">
                  <option value="IN" Selected>Pancaran</option>
                  <option value="EX">External</option>
                </select>
              </div>
              @if (Session::get('msg'))
              <span color="red" role="alert" style="color:red">
                {{Session::get('msg')}}
              </span>
              @endif
            </v-card-text>
            <v-card-actions>
              <v-layout row>
                <v-flex justify-center mb-2>
                  <v-btn type="submit" style="background-color: #23a9e1;
              font-family: 'Exo 2', sans-serif;
              border-radius: 15px;
              color: white;
              font-weight: 900;
              width:150px;
              height: 30px;">{{ __('i18n.login') }}</v-btn>
                </v-flex>
              </v-layout>
            </v-card-actions>
          </form>
        </v-card>
      </v-layout>
      <br>
      <div style="font-family: 'Exo 2', sans-serif; font-size:12px; color:#757575">
        <div>
          <a href="{{ url('locale/en') }}" style="color:#23a9e1;font-size:11px;">English</a> | <a href="{{ url('locale/id') }}" style="color:#23a9e1;font-size:11px;">Indonesia</a> | <a href="{{ url('locale/cn') }}" style="color:#23a9e1;font-size:11px;">China</a> | <a href="{{ url('locale/kr') }}" style="color:#23a9e1;font-size:11px;">Korea</a>
        </div>
        {{ __('i18n.forgotpassword') }}
        <a href="http://servicedesk.pancaran-group.co.id/" style="color:#23a9e1;font-size:11px;">{{ __('i18n.clickhere') }}</a>
        <a href="http://servicedesk.pancaran-group.co.id/" style="color:#23a9e1;margin-left:30px;font-size:11px;">{{ __('i18n.contactus') }}</a>
      </div>
      <div style="font-family: 'Exo 2', sans-serif; font-size:10px; color:#757575; padding-top:6%">
        <br><br>© {{ __('i18n.copyright') }}
        <br>{{ __('i18n.copyrightby') }}
      </div>
    </v-container>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify@1.5.0/dist/vuetify.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
  <script>
    var login = new Vue({
      el: '#app'
    })
  </script>
</body>

</html>
