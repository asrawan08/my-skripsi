<!DOCTYPE html>
<html style="overflow:hidden" lang="en">

<head>
  <!-- CSRF Token -->
  <title>Mobospace</title>
  <link rel="icon" type="image/ico" href="assets/home/favicon.ico" sizes="any" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta charset="utf-8">
  <meta http-equiv="Cache-Control" content="no-store"/>
  <!-- Scripts -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css')}}">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <link href='https://fonts.googleapis.com/css?family=Ubuntu:100,300,400,500,700,900|Material+Icons' rel="stylesheet" />
</head>

<body style="center fixed; 
  background-size: cover;
  overflow: hidden;">
<!-- <script src="{{ asset('js/hideConsole.js') }}" defer></script> -->
  <!-- background-image: linear-gradient(#A1F6FD, #0b5e7b,#2a1a44) -->
  <div id="app">
    <v-app style="background-color:#e3e3e3" >
      <v-content>
        <v-container fill-height align-start py-0 style="max-width:1960px;">
          <v-layout justify-center align-center py-0 my-0>
            <!-- <v-flex text-xs-center py-0 my-0 justify-center align-center> -->
            @yield('content')
            <!-- </v-flex> -->
          </v-layout>
        </v-container>
      </v-content>
      <v-footer color="transparent">
      </v-footer>
    </v-app>
  </div>
  @yield('scripts')
  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script> -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzgdorYLt8WgbE5D8kSa8z1qs1hDi1ciA"
  async defer></script> -->

  <!-- <script src="{{ asset('js/ApiUtil.js') }}" defer></script> -->
  <script src="{{ asset('js/app.js') }}" defer></script>
  <!-- <script src="{{ asset('js/Util.js') }}" defer></script> -->
</body>

</html>
