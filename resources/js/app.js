/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 require('jquery');
 
 window.Vue = require('vue');
 
 /**
  * The following block of code may be used to automatically register your
  * Vue components. It will recursively scan this directory for the Vue
  * components and automatically register them with their "basename".
  *
  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
  */
 
 // const files = require.context('./', true, /\.vue$/i)
 // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
 import Vuetify from './vuetify/vuetify'
 import Cookie from "js-cookie"
 import Vdebounce from "v-debounce"
 import * as VueGoogleMaps from 'vue2-google-maps'
 import VueGoogleCharts from 'vue-google-charts'
 
 Vue.use(VueGoogleCharts)
 
 Vue.use(VueGoogleMaps, {
     load: {
         key: 'AIzaSyBzgdorYLt8WgbE5D8kSa8z1qs1hDi1ciA',
         libraries: 'places',
     }
 })
 
 Vue.use(Cookie)
 Vue.component('menu-footer', require('./components/MenuFooter.vue').default);
 Vue.component('header-inbox', require('./components/HeaderInbox.vue').default);
 Vue.component('header-page', require('./components/HeaderPage.vue').default);
 Vue.component('notification-footer', require('./components/NotificationFooter.vue').default);
 
 Vue.component('search-field', require('./components/SearchField.vue').default);
 Vue.component('shipment-filter', require('./components/ShipmentFilter.vue').default);
 Vue.component('header-toggle', require('./components/HeaderToggle.vue').default);

 Vue.component('menu-footer', require('./components/MenuFooter.vue').default);
 Vue.component('monitoring', require('./components/Monitoring.vue').default);
 Vue.component('user-table', require('./components/monitoring/UserTable.vue').default);
 Vue.component('finger-print-table', require('./components/monitoring/FingerPrintTable.vue').default);
 Vue.component('rf-id-table', require('./components/monitoring/RfIdTable.vue').default);
 Vue.component('rf-id-report-table', require('./components/monitoring/RfIdReportTable.vue').default);
 Vue.component('user-access-report-table', require('./components/monitoring/UserAccessReportTable.vue').default);
 Vue.component('check-door-access-table', require('./components/monitoring/CheckDoorAccessTable.vue').default);

 Vue.component('moboconfig-rule-table', require('./components/monitoring/MoboConfigRuleTable.vue').default);
 Vue.component('moboconfig-action', require('./components/monitoring/MoboConfigAction.vue').default);
 Vue.component('moboconfig-form', require('./components/monitoring/MoboConfigForm.vue').default);
 Vue.component('moboconfig-output-form', require('./components/monitoring/MoboConfigOutputForm.vue').default);
 Vue.component('moboconfig-rule-form', require('./components/monitoring/MoboConfigRuleForm.vue').default);

 Vue.component('administration-table', require('./components/Administration.vue').default);
 Vue.component('administration-action', require('./components/administration/AdministrationAction.vue').default);
 Vue.component('administration-windowmanagementcontent', require('./components/administration/AdministrationWindowManagement.vue').default);
 Vue.component('administration-user-site', require('./components/administration/AdministrationUserSite.vue').default);
 Vue.component('administration-usermanagementcontent', require('./components/administration/AdministrationUserManagement.vue').default);
 Vue.component('administration-userprofilecontent', require('./components/administration/AdministrationProfile.vue').default);
 Vue.component('administration-form', require('./components/administration/AdministrationForm.vue').default);
 /**
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 const app = new Vue({
     vuetify: Vuetify,
     el: '#app',
     data: {
         cookie: Cookie
     }
 });