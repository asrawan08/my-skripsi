import Cookie from "js-cookie";
export default {
  isExpired : false,
  formatIdTime(dataTime){
    if(dataTime){
      var date = dataTime.split(/T|\s/);
      var aarDate = date[0];
      var arrTime = date[1];
      var getDate = aarDate.split("-")
      var getTime = arrTime.split(".")
      var getTimeReal = getTime[0].split(":")
      return getDate[2]+"/"+getDate[1]+"/"+getDate[0]+" "+getTimeReal[0]+":"+getTimeReal[1]+":"+getTimeReal[2];
    } 
    return "";
  }
}
