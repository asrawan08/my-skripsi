import Cookie from "js-cookie";

//var isExpired = false;
export default {
  isExpired: false,
  call(opts) {
    var dataReturn;
    if (!opts.ignoreConsoleLog) console.log("[CNApi.Call] Requesting API: ", opts)
    var bearer = opts.bearer || Cookie.get("token");
    var XClientId = Cookie.get("clientId");
    //var propitems = opts.directItemsProp ? ||"data.items";
    if (!opts.ignoreLoading) {
      console.log("loading true")
      $(".loading").show();
    }
    axios({
      method: opts.method || "post",
      url: opts.url,
      data: opts.getPostData ? opts.getPostData() : opts.postData,
      headers: opts.headers || {
        'Authorization': 'Bearer ' + bearer,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-Client-ID': XClientId,
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Credentials': true,
        // 'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS',
        // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Z-Key'
      }
    })
      .then(response => {
        if (!opts.ignoreConsoleLog) console.log("[CNApi.Call] Response Data = ", response.data);
        if (opts.beforeResult) {
          opts.beforeResult(response.data)
        }
        var dataItems = opts.directItemsProp ? response.data : (response.data ? (response.data.treeData || response.data.items) : []);
        if (opts.onEmpty && dataItems.length === 0) {
          console.log("DATA ITEMS IS EMPTY")
          opts.onEmpty()
        } else if (opts.onResult) {
          opts.onResult(response.data);
        } else if (opts.onItems) {
          opts.onItems(dataItems);
        } else if (opts.itemsToFill) {
          dataItems.forEach(item => opts.itemsToFill.push(item));
        } else if (opts.onEveryItem) {
          dataItems.forEach(item => opts.onEveryItem(item));
        } else if (opts.typeReturn) {
          dataReturn = response.data.items
        }
        if (opts.afterResult) {
          opts.afterResult(response.data);
        }
      }).catch(error => {
        console.log("[CNApi.Call] Error: ", error)
        $(".loading").hide();
        if (opts.onError) {
          opts.onError(error);
        }
        opts.errored = true
      }).finally(aa => {
        if (!opts.ignoreLoading) {
          $(".loading").hide();
        }
      });
  },
  action(opts) {

    console.log("[CNApi.Action] Opts: ", opts);

    this.checkSession(opts.ignoreCheckSession, () => {
      var bearer = opts.bearer || Cookie.get("token")
      console.log("[CNApi.Action] Bearer: ", bearer);
      if (!opts.ignoreLoading) {
        $(".loading").show();
      }
      axios({
        method: opts.method || "post",
        url: opts.url,
        data: opts.getPostData ? opts.getPostData() : opts.postData,
        headers: {
          'Authorization': 'Bearer ' + bearer,
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'X-Client-ID': Cookie.get("clientId")
        }
      })
        .then(response => {
          console.log("[CNApi.Action] Hasil update: ", response)
          if (opts.onSuccess) {
            opts.onSuccess(response.data);
          }
        }).catch(error => {
          console.log("[CNApi.Action] Error: ", error)
          // console.log("error",error.status)
          opts.errored = true
        }).finally(aa => {
          if (!opts.ignoreLoading) {
            $(".loading").hide();
          }
        });
    });
  },
  downloadFile(opts) {
    var bearer = opts.bearer || Cookie.get("token")
    axios({
      method: opts.method || "post",
      url: opts.url,
      data: opts.postData,
      headers: {
        'Authorization': 'Bearer ' + bearer,
        'Content-Type': 'application/json',
        'Accept': 'application/pdf',
        'X-Client-ID': Cookie.get("clientId"),
        'X-User': Cookie.get("username")
      },
      responseType: 'arraybuffer',
    })
      .then(response => {
        console.log("[CNApi.Action] Hasil update: ", response)
        // if(opts.onSuccess){
        //     opts.onSuccess(response.data);
        // }
        let blob = new Blob([response.data], { type: 'application/pdf' })
        let link = document.createElement('a')
        link.href = window.URL.createObjectURL(blob)
        link.download = opts.title + '.pdf'
        link.click()
      }).catch(error => {
        console.log("[CNApi.Action] Error: ", error)
        if (opts.onError) {
          opts.onError(error);
        }
        // console.log("error",error.status)
        opts.errored = true
      }).finally(aa => {
        if (!opts.ignoreLoading) {
          $(".loading").hide();
        }
      });
  },
  checkSession(ignoreCheckSession, executeAPI) {
    console.log('ignoreCheckSession=', ignoreCheckSession);
    if (ignoreCheckSession) {
      executeAPI();
      return;
    }
    console.log('check session');
    var me = this;
    fetch('home')
      .then((response) => {
        if (response.redirected == true) {
          location.replace("login");
          alert('Session timeout guys');
        } else if (executeAPI) {
          executeAPI();
        }
      })
      .catch((error) => {
        console.log('Looks like there was a problem: \n', error);
      });
  }
}
