<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
// use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
// use App\Modals\User;
use Validation;
// use Auth;
use Session;
use Cookie;
use Illuminate\Contracts\Session\Session as SessionSession;
use Redirect;
use Illuminate\Http\Request;

use App\getParams;

use Illuminate\Http\Response;
use SebastianBergmann\Environment\Console;

// use App\Http\Controllers\Session;

class AppController extends Controller
{
    public function index(Request $request)
    {
        if($this->checkSession()=='Y'){
            return redirect('/login');
        }
        else{
            return redirect('/login');
        }
    }
    public function proses(Request $req)
    {
        ini_set('date.timezone', 'Asia/Bangkok');
        $datetime         = date('Y-m-d H:i:s');

        $vUsername  = $req->email;
        $password   = $req->password;
        $logtype    = $req->logtype;
        setcookie("url_mobodfs",env('APP_URL_MOBODFS'));

        if ($logtype=="EX")
        {
            $userlist = DB::table('manageuser')
                        ->where('username', '=', $vUsername)
                        ->where('password', '=', md5($password))
                        ->where('status', '=', 'Y')
                        ->get();
            $userCount = $userlist->count();
            if($userCount > 0)
            {
                $user = DB::table('manageuser')
                        -> select('manageuser.*', 'client.name as c_name','client.*')
                        -> join('client', 'manageuser.client_id', '=', 'client.client_id')
                        ->where('username', $vUsername)->first();
                $vMember     = $user->memberis;
                $vPhone     = $user->phone;
                $vEmails    = $user->emails;
                $vPassword = $user->password;
                $vErpId = $user->erp_id;
                $vEmployeer = $user->fullname;
                $vfullname  = $user->fullname;
                $vPosition  = $user->position;
                $vClientId  = $user->client_id;
                $vNameClient = $user->c_name;
                $roleId = $user->role_id;
                $vUsernameApi = $user->user_api;
                $vPasswordApi = $user->password_api;
                $vConsumerKey = $user->consumer_key;
                $vConsumerSecret = $user->consumer_secret;
                $result =  $this->getToken($vConsumerKey,$vConsumerSecret,$vUsernameApi,$vPasswordApi);
                $json         = json_decode($result);
                $cekSession   = (isset($json->error));

                if($cekSession=="invalid_grant")
                {
                    Session::put('msg','consumer Key or Consumer Secret is Wrong');
                    return redirect('/login');
                }else{
                    $token            = $json->access_token;
                    $refresh_token    = $json->refresh_token;
                    $scope            = $json->scope;
                    $token_type       = $json->token_type;
                    $expires_in       = $json->expires_in;
                    //setcookie("MoboToken",$token,time()+3600);
                    //setcookie("MoboClientId",$vClientId,time()+3600);
                    setcookie("role",$roleId);
                    setcookie("token","ab770253-4ee4-3a14-a450-d231a97ee303");
                    setcookie("clientId",$vErpId);
                    setcookie("username",$vUsername);

                    $sesdata    = array(
                        'username'     => $vUsername,
                        'nama'         => $vfullname,
                        'clientid'     => $vClientId,
                        'clientname'   => $vNameClient,
                        'departemen'   => $vPosition,
                        'employee'     => $vEmployeer,
                        'memberis'     => $vMember,
                        'logged_in'    => TRUE,
                        'logtype'      => $logtype
                    );

                    DB::table('client')->where('client_id','=', $vClientId)->update([
                        'refresh_token'     => $refresh_token,
                        'access_token'      => $token
                    ]);

                    DB::table('manageuser')->where('username','=', $vUsername)->update([
                        'last_access'     => $datetime
                    ]);

                    $wordlist = DB::table('user_log')
                                ->where('username', '=', $vUsername)
                                ->get();
                    $wordCount = $wordlist->count();

                    if($wordCount > 0)
                    {
                        DB::table('user_log')->where('username','=', $vUsername)->update([
                            'access_token'    => $token,
                            'refresh_token'   => $refresh_token,
                            'scope'           => $scope,
                            'token_type'      => $token_type,
                            'expires_in'      => $expires_in,
                            'last_access'     => $datetime,
                            'updated_at' => now()
                        ]);
                    }else{
                        DB::table('user_log')->insert([
                            'id' => (collect(DB::select('select get_uuid() AS nb'))->first()->nb),
                            'username'        => $vUsername,
                            'access_token'    => $token,
                            'refresh_token'   => $refresh_token,
                            'scope'           => $scope,
                            'token_type'      => $token_type,
                            'expires_in'      => $expires_in,
                            'last_access'     => $datetime,
                            'logtype'         => $logtype,
                            'created_at' => now()
                        ]);
                    }
                    Session::put('datases', $sesdata);
                    return redirect('/home');
                }
            }else{
                Session::put('msg','Username or Password is Wrong');
                return redirect('/login');
            }
        }else{
            //ConsumerKey dan ConsumerSecret App. TMAWeb Production
                //$vConsumerKey="RT0FoTCzRs0qqrqmCN3z50uc9cYa";
                //$vConsumerSecret="WBFjLCeNJxfEHlRHfcXiq5iKDREa";

            //ConsumerKey dan ConsumerSecret App. TMAWeb Sandbox
                $vConsumerKey="lRVvJmtwfILfbFJvXXQmInITrjka";
                $vConsumerSecret="tBPCDIJFyigUJqlTjQPwfeRVVx4a";


            $vClientIdPancaran = "2C7749C5E9BC4E21AED0593573BEC709";
            $roleId="PANCARAN";

            $result =  $this->getToken($vConsumerKey,$vConsumerSecret,$vUsername,$password);
            $json         = json_decode($result);
            $cekSession   = (isset($json->error));

            if($cekSession=="invalid_grant")
            {
                Session::put('msg','Username or Password is Wrong');
                return redirect('/login');
            }else{
                $token            = $json->access_token;
                $refresh_token    = $json->refresh_token;
                $scope            = $json->scope;
                $token_type       = $json->token_type;
                $expires_in       = $json->expires_in;
                //setcookie("MoboToken",$token,time()+3600);
                //setcookie("MoboClientId",$vClientIdPancaran,time()+3600);
                setcookie("role",$roleId,time() + (10 * 365 * 24 * 60 * 60));
                setcookie("token","ab770253-4ee4-3a14-a450-d231a97ee303",time() + (10 * 365 * 24 * 60 * 60));
                setcookie("clientId",$vClientIdPancaran,time() + (10 * 365 * 24 * 60 * 60));
                setcookie("username",$vUsername,time() + (10 * 365 * 24 * 60 * 60));

                $result =  $this->getScim2Me($token);
                $json = json_decode($result);

                $status = (isset($json->status));
                echo $status;
                if ($status == NULL)
                {
                    $vEmails = (isset($json->emails[0]));
                        if ($vEmails == NULL) {
                            $vEmails = "";
                        }else{
                            $vEmails = $json->emails[0];
                        }
                    $vDisplayName = (isset($json->displayName));
                        if ($vDisplayName == NULL) {
                            $vDisplayName = "";
                        }else{
                            $vDisplayName = $json->displayName;
                        }
                    $vRolesValue = (isset($json->roles[0]->value));
                        if ($vRolesValue == NULL) {
                            $vRolesValue = "";
                        }else{
                            $vRolesValue = $json->roles[0]->value;
                        }
                    $vDepartment = (isset($json->EnterpriseUser->department));
                        if ($vDepartment == NULL) {
                            $vDepartment = "";
                        }else{
                            $vDepartment = $json->EnterpriseUser->department;
                        }
                    $employeeNumber = (isset($json->EnterpriseUser->employeeNumber));
                    if ($employeeNumber == NULL) {
                        $employeeNumber = "";
                    }else{
                        $employeeNumber = $json->EnterpriseUser->employeeNumber;
                    }
                    $vFirstName = (isset($json->name->givenName));
                    if ($vFirstName == NULL) {
                        $vFirstName = "";
                    }else{
                        $vFirstName = $json->name->givenName;
                    }
                    $vLastName = (isset($json->name->familyName));
                    if ($vLastName == NULL) {
                        $vLastName = "";
                    }else{
                        $vLastName = $json->name->familyName;
                    }
                    $vExternalId = (isset($json->externalId));
                    if ($vExternalId == NULL) {
                        $vExternalId = "";
                    }else{
                        $vExternalId = $json->externalId;
                    }
                    $vId = (isset($json->id));
                    if ($vId == NULL) {
                        $vId = "";
                    }else{
                        $vId = $json->id;
                    }
                    $vPancaranUserName = (isset($json->userName));
                    if ($vPancaranUserName == NULL) {
                        $vPancaranUserName = "";
                    }else{
                        $vPancaranUserName = $json->userName;
                    }
                    $vPhoneValue = (isset($json->phoneNumbers[0]->value));
                    if ($vPhoneValue == NULL) {
                        $vPhoneValue = "";
                    }else{
                        $vPhoneValue = $json->phoneNumbers[0]->value;
                    }

                    $sesdata    = array(
                        'username'     => $vUsername,
                        'nama'         => $vUsername,
                        'clientid'     => $vClientIdPancaran,
                        'clientname'   => 'PANCARAN GROUP',
                        'departemen'   => $vDepartment,
                        'employee'     => $vPancaranUserName,
                        'memberis'     => 'PG',
                        'logtype'      => $logtype,
                        'logged_in'    => TRUE
                    );
                    // setcookie("token","a8a3b546-40af-3e13-bf3a-aa460b5cfa5c"); //Untuk Production
                    //setcookie("token","ab770253-4ee4-3a14-a450-d231a97ee303"); //Untuk Sandbox
                    $wordlist = DB::table('user_log')
                    ->where('username', '=', $vUsername)
                    ->get();
                    $wordCount = $wordlist->count();

                    if($wordCount > 0) {
                        DB::table('user_log')->where('username',$vUsername)->update([
                            'access_token'    => $token,
                            'refresh_token'   => $refresh_token,
                            'scope'           => $scope,
                            'token_type'      => $token_type,
                            'expires_in'      => $expires_in,
                            'last_access'     => $datetime,
                            'updated_at' => now()
                        ]);
                    }else{
                        DB::table('user_log')->insert([
                            'id' => (collect(DB::select('select get_uuid() AS nb'))->first()->nb),
                            'username'        => $vUsername,
                            'access_token'    => $token,
                            'refresh_token'   => $refresh_token,
                            'scope'           => $scope,
                            'token_type'      => $token_type,
                            'expires_in'      => $expires_in,
                            'last_access'     => $datetime,
                            'logtype'         => $logtype,
                            'created_at' => now()
                        ]);
                    }

                    Session::put('datases', $sesdata);
                    return redirect('/home');

                }else{
                    Session::put('msg','Access Denied');
                    return redirect('/login');

                }
            }
        }
    }


    public function coba(){
        //echo Session::get('datases')['username'];
        if($this->checkSession()=='Y'){
            echo "Masukin";
        }else{
            echo "Keluarin";
        };
    }

    function checkSession(){
        $vUsername =  Session::get('datases')['username'];
        $wordlist = DB::table('user_log')->where('username', '=', $vUsername)->get();
        $wordCount = $wordlist->count();

        if($wordCount > 0) {
            $status = "Y";
        }else{
            $status = "N";
        }
        return $status;
    }


    function getToken($vConsumerKey,$vConsumerSecret,$vUsername,$vPassword){
                $vData =  $vConsumerKey . ":" . $vConsumerSecret;
                $resultBase = base64_encode($vData);

                $url      = "http://centralnode.pancaran-group.co.id:8282/token";
                $body     = array("grant_type"=>"password",
                    "username"    => $vUsername,
                    "password"    => $vPassword,
                    "scope"       => "openid"
                    );
                $build    = http_build_query($body);
                $header   = array(
                "Authorization:Basic ".$resultBase,
                "Content-Type:application/x-www-form-urlencoded");
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $build);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
                return $result;
    }

    function getScim2Me($token){
        $url = "https://centralnode.pancaran-group.co.id:9447/scim2/Me";
        $header = array("Authorization:Bearer ".$token."");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
}

    public function refreshToken($vConsumerKey,$vConsumerSecret,$vRefreshToken){
                $vData =  $vConsumerKey . ":" . $vConsumerSecret;
                $resultBase = base64_encode($vData);

                $url      = "https://api.pancaran-group.co.id/token";
                $body     = array(
                        "grant_type"    =>"refresh_token",
                        "refresh_token" =>$vRefreshToken
                        );
                $build    = http_build_query($body);
                $header   = array(
                "Authorization:Basic ".$resultBase,
                "Content-Type:application/x-www-form-urlencoded");
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $build);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
                return $result;

    }

    public function driver()
    {
        if($this->checkSession()=='Y'){
            return view('driver');
        }
        else{
            return redirect('/login');
        }
    }

    public function monitoringlog()
    {
        if($this->checkSession()=='Y'){
            return view('monitoringCheckPoint');
        }
        else{
            return redirect('/login');
        }
    }

    public function dashboard()
    {
        $vLink = "dashboard";
        $vUserName = Session::get('datases')['username'];
        $logtype = Session::get('datases')['logtype'];
        
        $data = DB::table('menu as a')
                -> select('b.menu as parent_menu')
                -> where('manageuser.username', '=', $vUserName)
                -> where('b.menu','=', $vLink)
                -> join('permission','a.menu_id','=','permission.menu_id')
                -> join('manageuser','manageuser.manageuser_id','=','permission.manageuser_id')
                -> leftJoin('menu as b', 'a.parent_id','=','b.menu_id')
                -> get();

                
        if($this->checkSession()=='Y'){
            if($data->count() > 0 || $logtype == 'IN')
                return view($vLink);
            else
                return view('home');
        }
        else{
            return redirect('/login');
        }
    }
    public function order()
    {
        if($this->checkSession()=='Y'){
            return view('info_order');
        }
        else{
            return redirect('/login');
        }
    }
    public function notificationMessage()
    {
        if($this->checkSession()=='Y'){
            return view('notificationMessage');
        }
        else{
            return redirect('/login');
        }
    }
    public function home()
    {
        if($this->checkSession()=='Y'){
            $vUserName = Session::get('datases')['username'];
            $logtype = Session::get('datases')['logtype'];
            
            $operationlDashboard = 'operationaldashboard';
            $mobomap = 'mobomap';
            $shipmentdetail = 'shipmentdetail';
            $mobodriver  = 'mobodrive';
            $verifyactivity  = 'verifyactivity';
            $dashboardactivity  = 'dashboardactivity';
            $notificationmessage  = 'notificationmessage';
            $inspectionResult  = 'inspectionresult';

            $bOperationlDashboard = false;
            $bMobomap = false;
            $bShipmentdetail = false;
            $bMobodriver = false;
            $bVerifyActivity = false;
            $bDashboardActivity = false;
            $bNotifMessage = false;
            $bInspectionResult = false;

            $data = DB::table('menu as a')
                    -> select('a.link')
                    -> join('permission','a.menu_id','=','permission.menu_id')
                    -> join('manageuser','manageuser.manageuser_id','=','permission.manageuser_id')
                    -> leftJoin('menu as b', 'a.parent_id','=','b.menu_id')
                    -> where('manageuser.username', '=', $vUserName)
                    -> where('a.status', '=', 'Y')
                    -> get();
                    
            if ($logtype == "EX") {
                for ($i = 0; $i < $data->count(); $i++) {
                    if ($operationlDashboard == $data[$i]->link)
                        $bOperationlDashboard = true;
                    if ($mobomap == $data[$i]->link)
                        $bMobomap = true;
                    if ($shipmentdetail == $data[$i]->link)
                        $bShipmentdetail = true;
                    if ($mobodriver == $data[$i]->link)
                        $bMobodriver = true;
                    if ($bVerifyActivity == $data[$i]->link)
                        $bVerifyActivity = true;
                    if ($dashboardactivity == $data[$i]->link)
                        $bDashboardActivity = true;
                    if ($notificationmessage == $data[$i]->link)
                        $bNotifMessage = true;
                    if ($inspectionResult == $data[$i]->link)
                        $bInspectionResult = true;
                }
            } else if ($logtype == "IN") {
                $bOperationlDashboard = true;
                $bMobomap = true;
                $bShipmentdetail = true;
                $bMobodriver = true;
                $bVerifyActivity = true;
                $bDashboardActivity = true;
                $bNotifMessage = true;
                $bInspectionResult = true;
            }

            return view('home', compact('bOperationlDashboard', 'bShipmentdetail', 'bMobomap', 'bMobodriver',
                        'bVerifyActivity', 'bDashboardActivity', 'bNotifMessage', 'bInspectionResult'));
        }
        else{
            return redirect('/login');
        }
    }
    public function inbox()
    {
        if($this->checkSession()=='Y'){
            return view('inbox');
        }
        else{
            return redirect('/login');
        }
    }
    public function shipmentLocation()
    {

        if($this->checkSession()=='Y'){
            // return view('shipmentLocation');
            // return view('errors/underConstruction');
            return view('liveTracking');
        }
        else{
            return redirect('/login');
        }
    }
    public function shipmentActivity()
    {
        if($this->checkSession()=='Y'){
            return view('shipment_activity');
        }
        else{
            return redirect('/login');
        }
    }
    public function moboConfig()
    {
        if($this->checkSession()=='Y'){
            return view('monitoring');
        }
        else{
            return redirect('/login');
        }
    }
    public function dashboardActivity()
    {
        if($this->checkSession()=='Y'){
            return view('dashboard_activity');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }
    public function mobodrivePersonalDocument()
    {
        if($this->checkSession()=='Y'){
            return view('mobodrive_personal_document');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }
    public function viewdetailnotification()
    {
        if($this->checkSession()=='Y'){
            return view('viewdetailnotification');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }
    public function moboReportManager()
    {
        if($this->checkSession()=='Y'){
            return view('moboreport_manager');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }
    public function reportManagement()
    {
        if($this->checkSession()=='Y'){
            return view('report_management');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function styledReportManager()
    {
        if($this->checkSession()=='Y'){
            return view('styled_report_manager');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function ldapHrisMappingTools()
    {
        if($this->checkSession()=='Y'){
            return view('ldap_hris_mapping_tools');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function employmateAccountCandidate()
    {
        if($this->checkSession()=='Y'){
            return view('employmate_account_candidate');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function employmateAccountManagement()
    {
        if($this->checkSession()=='Y'){
            return view('employmate_account_management');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function operationalDashboard()
    {
        if($this->checkSession()=='Y'){
            return view('operational_dashboard');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function mobodriveInfo()
    {
        if($this->checkSession()=='Y'){
            return view('information_content');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function logasConfig()
    {
        if($this->checkSession()=='Y'){
            return view('logas_config');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }

    public function eventPublisher()
    {
        if($this->checkSession()=='Y'){
            return view('event_publisher');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }
    public function popupUpload(Request $request)
    {
        //echo $request->input('event');
        Session::put('event', $request->input('sheetKey'));
        Session::put('parent_id', $request->input('recordId'));
        Session::put('client', $request->input('client'));
        Session::put('token','ab770253-4ee4-3a14-a450-d231a97ee303');
        Session::put('username', $request->input('user'));
        Session::put('org', $request->input('org'));
        Session::put('extra_massage', $request->input('extra_massage'));
        setcookie("eventPopup",$request->input('sheetKey'));
        return view('popupExternal');

    }
    public function styledUploadManager(){
        if($this->checkSession()=='Y'){
            return view('styled_upload_manager');
        }
        else{
            return redirect('/login');
        }
    }

    public function moboUploadManager(){
        if($this->checkSession()=='Y'){
            return view('moboupload_manager');
        }
        else{
            return redirect('/login');
        }
    }

    public function vhcGpsMapping()
    {
        if($this->checkSession()=='Y'){
            return view('vehicle_gps_mapping');
            // return view('errors/underConstruction');
        }
        else{
            return redirect('/login');
        }
    }
    
    public function gpsUnitList()
    {
        if($this->checkSession()=='Y'){
            return view('gps_unit_list');
        }
        else{
            return redirect('/login');
        }
    }

    public function gpsConfig()
    {
        if($this->checkSession()=='Y'){
            return view('gps_config');
        }
        else{
            return redirect('/login');
        }
    }

    public function logout()
    {
        $vUserName = Session::get('datases')['username'];
        $vLogType = Session::get('datases')['logtype'];
        if($this->checkSession()=='Y'){
            if(Session::get('msg')){
                Session::forget('msg');
                DB::table('user_log')
                    ->where('username',$vUserName)
                    ->where('logtype',$vLogType)
                    ->delete();
            }
            Session::forget('datases');
            DB::table('user_log')
                ->where('username',$vUserName)
                ->where('logtype',$vLogType)
                ->delete();
            Artisan::call('cache:clear');
            return redirect('/login');
	    }else{
            Artisan::call('cache:clear');
            return redirect('/login');
        }
    }

    public function saveuser(Request $request)
    {
        if($this->checkSession()=='Y'){
            $vLogType = Session::get('datases')['logtype'];
            if($vLogType=="IN"){
                $vClientID = $request->client_id;
                $vMemberIs = $request->memberis;
            }else{
                $vClientID = Session::get('datases')['clientid'];
                $vMemberIs = "Member";
            }
            
            $vUsername = $request->username;

            $jmlMax = DB::table('client')->where('client_id', $vClientID)->first();
            $maxUser = $jmlMax->max_user;

            $wordlist = DB::table('manageuser')->where('client_id', '=', $vClientID)->get();
            $jmlUser = $wordlist->count();

            if ( $jmlUser >= $maxUser) {
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'Maximal Limit User, Count User ' . $jmlUser . "/".  $maxUser
                ]);
            }else{
                $wordlist = DB::table('manageuser')
                            ->where('username', '=', $vUsername)
                            ->get();
                $wordCount = $wordlist->count();

                $adminList = DB::table('manageuser')
                        ->where('client_id', '=', $vClientID)
                        ->where('memberis', '=', 'Admin')
                        ->get();
                $adminCount = $adminList->count();
                
                if($adminCount == 0 && $vMemberIs != "Admin") {                      
                    return response()->json([
                        'type' => 'ERROR',
                        'message' => 'Cant Create User Member Before Create User Admin First'
                    ]);
                }

                if($wordCount > 0) {
                    return response()->json([
                        'type' => 'ERROR',
                        'message' => 'Username Already Exist '
                    ]);
                }else{
                    $uuiduser = (collect(DB::select('select get_uuid() AS nb'))->first()->nb);
                    DB::table('manageuser')->insert([
                        'manageuser_id' => $uuiduser,
                        'fullname' => $request->fullname,
                        'erp_id' => $request->erp_id,
                        'phone' => $request->phone,
                        'emails' => $request->emails,
                        'position' => $request->position,
                        'username' => $request->username,
                        'password' => md5($request->password),
                        'status' => $request->status,
                        'memberis' => $vMemberIs, 
                        'client_id' => $vClientID,
                        'created_at' => now(),
                        'createdby' => $request->createdby
                    ]);

                    $results = DB::table('manageuser')->where('username', $request->username)->first();

                    $menu  = $request->menu;
                    $cMenu = count($menu);

                    DB::table('permission')->where('manageuser_id', $request->manageuser_id)->delete();

                    for($i=0; $i<$cMenu; $i++){
                        DB::table('permission')->insert([
                            'permission_id' =>  (collect(DB::select('select get_uuid() AS nb'))->first()->nb),
                            'menu_id' => $request->menu[$i],
                            'client_id' => $vClientID,
                            'manageuser_id' => $results->manageuser_id
                        ]);
                    }

                    $wordlist = DB::table('manageuser')->where('client_id', '=', $vClientID)->get();
                    $userSaved = $wordlist->count();

                    return response()->json([
                        'type' => 'SUCCESS',
                        'message' => 'Data Saved, Count User ' . $userSaved . "/".  $maxUser,
                        'idUser' => $uuiduser
                    ]);
                }
            }
        }else{
            return redirect('/login');
        }
    }

    public function updateuser(Request $request)
    {
        if($this->checkSession()=='Y'){

            $password = $request->password;     
            $oldpassword = $request->oldpassword;    
            
            $mngsrc = DB::table('manageuser')->where('manageuser_id', $request->manageuser_id)->first();
            $vUser = $mngsrc->username;

            $wordlist = DB::table('user_log')->where('username', '=', $vUser)->get();
            $wordCount = $wordlist->count();

            if($wordCount > 0) {
                return response()->json([
                        'type' => 'ERROR',
                        'message' => 'User Online.'
                ]);
            }else if(strlen($password) > 0 && strlen($password) < 8  ) {
                    return response()->json([
                            'type' => 'ERROR',
                            'message' => 'Password minimal 8 Character'
                    ]);
            }else{
                DB::table('manageuser')->where('manageuser_id',$request->manageuser_id)->update([
                    'fullname' => $request->fullname,
                    'phone' => $request->phone,
                    'emails' => $request->emails,
                    'position' => $request->position,
                    'username' => $request->username,
                    'password' => $password == null ? $oldpassword:md5($password),
                    'status' => $request->status,
                    'erp_id' => $request->erp_id,
                    'role_id' => $request->role_id,
                    'client_id' => $request->client_id,
                    'memberis' => $request->groupUser,
                    'updated_at' => now(),
                    'updatedby' => $request->updatedby
                ]);
                
                DB::table('permission')->where('manageuser_id', $request->manageuser_id)->delete();

                $menu  = $request->menu;
                $cMenu = count($menu);

                for($i=0; $i<$cMenu; $i++){
                    DB::table('permission')->insert([
                        'permission_id' =>  (collect(DB::select('select get_uuid() AS nb'))->first()->nb),
                        'menu_id' => $request->menu[$i],
                        'client_id' => $request->client_id,
                        'manageuser_id' => $request->manageuser_id
                    ]);
                }
                return response()->json([
                    'type' => 'SUCCESS',
                    'message' => 'Data Updated',
                    'idUser' => $request->manageuser_id
                ]);
            }
        }else{
            return redirect('/login');
        }
    }

    public function updatewindow(Request $request)
    {   
        if($this->checkSession()=='Y'){
        
            $wordlist = DB::table('menu')
            ->where('menu', '=', $request->menu)
            ->where('menu_id', '!=', $request->menu_id)
            ->get();
            $wordCount = $wordlist->count();

            if($wordCount > 0) {
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'Menu Name Already Exist '
                ]);
            }

            $wordlist = DB::table('menu')
                        ->where('menu', '=', $request->menu)
                        ->get();
            $wordCount = $wordlist->count();

            if($request->link != null) {
                
                $wordlist = DB::table('menu')
                ->where('link', '=', $request->link)
                ->where('menu_id', '!=', $request->menu_id)
                ->get();   
                $wordCount = $wordlist->count();

                if($wordCount > 0) {
                    return response()->json([
                        'type' => 'ERROR',
                        'message' => 'Window With This Link Already Exist'
                    ]);
                }
            }

            DB::table('menu')->where('menu_id',$request->menu_id)->update([
                'menu' => $request->menu,
                'link' => $request->link,
                'status' => $request->status,
                'parent_id' => $request->parent_id,
                'updatedby' => $request->updatedby,
                'updated' => now()
            ]);

            return response()->json([
                'type' => 'SUCCESS',
                'message' => 'Data Updated'
            ]);
        }
        else {
            return redirect('/login');
        }
    }

    public function updateprofile(Request $request)
    {

        if($this->checkSession()=='Y'){
            $cekoldpassword = $request->oldpassword;
            $oldpasswd = $request->password;
            $newpasswd = $request->newpassword;
            $hasnewpasswd = md5($newpasswd);
            $conpasswd = $request->confirmpassword;
            $newpwd = $oldpasswd;

            if ($oldpasswd != "" && ((md5($oldpasswd) != $cekoldpassword))){
                    return response()->json([
                        'type' => 'ERROR',
                        'message' => 'Old Password Wrong'
                    ]);
            }else if ($oldpasswd != "" && $newpasswd==""){
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'New Password is Empty.'
                ]);
            }else  if ($oldpasswd != "" && $conpasswd==""){
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'Confirm Password is Empty.'
                ]);
            }else if($oldpasswd != "" && ($conpasswd != $newpasswd)){
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'New Password and Confirm Password is Not Same.'
                ]);
            }else if($oldpasswd != "" && (strlen($newpasswd) < 8 )){
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'New Password Min. 8 Character.'
                ]);
            }else if($oldpasswd != "" && ($cekoldpassword == $hasnewpasswd)){
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'Old Password same with same password.'
                ]);
            }else{
                $password = $oldpasswd;
                DB::table('manageuser')->where('manageuser_id',$request->manageuser_id)->update([
                    'fullname' => $request->fullname,
                    'phone' => $request->phone,
                    'emails' => $request->email,
                    'position' => $request->position,
                    'password' => ($oldpasswd == null)? $cekoldpassword : md5($newpasswd)
                    ]);
                return response()->json([
                    'type' => 'SUCCESS',
                    'message' => 'Data Profile Updated.'
                ]);
            }
        }else{
            return redirect('/login');
        }
    }

    public function deleteuser(Request $request)
    {
        if ($this->checkSession() == 'Y') {
            $cManageuser = count($request->manageuser_id);
            $cDeleted = 0;
            $message = "";
            $bOnline = false;
            $userOnline = "";
            $type = "";
            
            for ($i = 0; $i < $cManageuser; $i++) {
                $mngsrc = DB::table('manageuser')->where('manageuser_id', $request->manageuser_id[$i])->first();
                $vUser = $mngsrc->username;

                $wordlist = DB::table('user_log')->where('username', '=', $vUser)->get();
                $wordCount = $wordlist->count();

                if ($wordCount > 0) {
                    $bOnline = true;
                    $userOnline .= $request->username[$i] . ",";
                } else {
                    DB::table('user_log')->where('username', $vUser)->delete();
                    DB::table('permission')->where('manageuser_id', $request->manageuser_id[$i])->delete();
                    DB::table('manageuser')->where('manageuser_id', $request->manageuser_id[$i])->delete();

                    $cDeleted += 1;
                }
            }
            
            $userOnline = substr($userOnline, 0, -1);

            if($cDeleted > 0 && $cDeleted == 1) {
                $message .= $cDeleted . " user is deleted";
                $type = "SUCCESS";
            }
            else if ($cDeleted > 1) {
                $message .= $cDeleted . " users is deleted";
                $type = "SUCCESS";
            }
            if($cDeleted > 0 && $bOnline)
                $message .=  ". ";
            if($bOnline)
                $message .= "User: " . $userOnline ." is online";
            if ($bOnline && $cDeleted == 0)
              $type = "ERROR";

            return response()->json([
                'type' => $type,
                'message' => $message
            ]);
        
        }else{
            return redirect('/login');
        }
    }


    public function killsession(Request $request)
    {
        if($this->checkSession()=='Y'){
            $mngsrc = DB::table('manageuser')->where('manageuser_id', $request->manageuser_id)->first();
            $vUser = $mngsrc->username;

            $wordlist = DB::table('user_log')->where('username', '=', $vUser)->get();
            $wordCount = $wordlist->count();

            if($wordCount > 0) {
                DB::table('user_log')->where('username',$vUser)->delete();
                return response()->json([
                        'type' => 'SUCCESS',
                        'message' => 'User Session Kill.'
                ]);
            }else{
                return response()->json([
                        'type' => 'ERROR',
                        'message' => 'User Not Online.'
                ]);
            }
        }else{
            return redirect('/login');
        }
    }


    public function setting()
    {
        if($this->checkSession()=='Y'){
            $data['manageuser'] = DB::table('manageuser')
            -> select('managerole.*','manageuser.*', 'client.name as c_name',
                  DB::raw("(SELECT logtype FROM user_log
                          WHERE user_log.username = manageuser.username limit 1
                          ) as logtype"))
              -> join('managerole', 'manageuser.role_id', '=', 'managerole.role_id')
              -> join('client', 'manageuser.client_id', '=', 'client.client_id')
              -> orderBy('manageuser.client_id', 'asc')
              -> orderBy('manageuser.memberis', 'asc')
              -> get();
            $data['managerole'] = DB::table('managerole')->get();
            $data['client'] = DB::table('client')->get();
            return view('setting',$data);
        }else{
            return redirect('/login');
        }
    }


    public function saverole(Request $request)
    {
        if($this->checkSession()=='Y'){
            $vName = $request->name;
            $wordlist = DB::table('managerole')
                        ->where('name', '=', $vName)
                        ->get();
            $wordCount = $wordlist->count();

            if($wordCount > 0) {
                return redirect('/setting')->with(['error' => 'Username Already Exist']);
            }else{
                $vClientID = Session::get('datases')['username'];
                DB::table('managerole')->insertinsert([
                    'role_id' => (collect(DB::select('select get_uuid() AS nb'))->first()->nb),
                    'name' => $request->name,
                    'description' => $request->description,
                    'created_at' => now()
                ]);
                return redirect('/setting')->with(['success' => 'Data Saved']);
            }
        }else{
            return redirect('/login');
        }
    }

    public function updaterole(Request $request)
    {
        if($this->checkSession()=='Y'){
            $vClientID = Session::get('datases')['username'];
            DB::table('managerole')->where('role_id',$request->role_id)->update([
                'name' => $request->name,
                'description' => $request->description,
                'updated_at' => now()
            ]);
            return redirect('/setting')->with(['success' => 'Data Updated']);;
        }else{
            return redirect('/login');
        }
    }

 public function deleterole($role_id)
    {
        if($this->checkSession()=='Y'){
            DB::table('managerole')->where('role_id',$role_id)->delete();
            return redirect('/setting')->with(['success' => 'Data Deleted']);;
        }else{
            return redirect('/login');
        }
    }

    //customer Panel Area
    public function administration()
    {
        if($this->checkSession()=='Y'){
            $data['managerole'] = DB::table('managerole')->get();
            $data['client'] = DB::table('client')->get();
            return view('administration',$data);
        }else{
            return redirect('/login');
        }
    }

    public function reloadusers(Request $request)
    {
        if($this->checkSession()=='Y'){

            $manageuser = $request->erp_id;
            if($manageuser==""){
                $param = "like";
                $condition = "%%";
            }else{
                $param = "=";
                $condition = $manageuser;
            }
            $data ['reloadusers'] = DB::table('manageuser')
                             -> select('manageuser.*', 'client.name as c_name','client.erp_id',
                                DB::raw("(SELECT logtype FROM user_log
                                    WHERE user_log.username = manageuser.username limit 1
                                    ) as logtype"))
                             //-> join('managerole', 'manageuser.role_id', '=', 'managerole.role_id')
                             -> join('client', 'manageuser.client_id', '=', 'client.client_id')
                             -> where('client.erp_id',$param, $condition)
                             -> get();
        return response()->json($data);
        }else{
            return redirect('/login');
        }
    }

    public function getparams()
    {
        $logtype = Session::get('datases')['logtype'];
        $username = Session::get('datases')['username'];

        $data ['menu'] = DB::table('menu')
                         ->select('menu.*')
                         ->orderByRaw('menu ASC')
                         ->where('parent_id','!=','#')
                         ->where('status','=', 'Y')
                         ->get();

        $data ['customer'] = DB::table('client')
                             -> select('client_id','name','erp_id')
                             -> orderByRaw('name ASC')
                             -> where('active','=', 'Y')
                             -> get();

        $data ['usermenufilter'] = DB::table('menu as a')
                    -> select('a.menu_id','a.menu', 'a.link', 'b.menu as parent_menu', 'b.menu_id as parent_id')
                    -> where('manageuser.username', '=', $username)
                    -> where('a.status','=', 'Y')
                    -> join('permission','a.menu_id','=','permission.menu_id')
                    -> join('manageuser','manageuser.manageuser_id','=','permission.manageuser_id')
                    -> leftJoin('menu as b', 'a.parent_id','=','b.menu_id')
                    -> get();
        
        $data ['logtype'] = $logtype;

        return response()->json($data);
    }

    public function getprofile() {
        $data = DB::table('manageuser')
        -> select('manageuser.*', 'client.name as c_name',
        DB::raw("(SELECT logtype FROM user_log
            WHERE user_log.username = manageuser.username limit 1
            ) as logtype"))
        -> join('client', 'manageuser.client_id', '=', 'client.client_id')
        -> where('manageuser.username', Session::get('datases')['username'])
        -> get();

        return response()->json($data);
    }

    public function getuserbymember() {
        $logtype = Session::get('datases')['logtype'];
        $username = Session::get('datases')['username'];
        $memberis = DB::table('manageuser') -> select('manageuser.memberis')
                    ->where('manageuser.username', '=', $username);
                    
        if ($logtype=="EX") {
            if ($memberis == "Admin") {
                $paramMemberIs = '=';
                $whereMemberIs = 'Member';
            }
            $paramMemberIs = '=';
            $whereMemberIs = 'Member';
            $paramClientID = "=";
            $whereClientID = Session::get('datases')['clientid'];
            $paramUsername = "!=";
            $whereUsername = $username;
        }else{
            $paramClientID = "like";
            $whereClientID = "%%";
            $paramUsername = "like";
            $whereUsername = "%%";
            $paramMemberIs = 'like';
            $whereMemberIs = '%%';
        }

        $data ['userlist'] = DB::table('manageuser')
                             -> select('manageuser.*', 'client.name as c_name','client.erp_id',
                                DB::raw("(SELECT logtype FROM user_log
                                    WHERE user_log.username = manageuser.username limit 1
                                    ) as logtype"))
                             -> join('client', 'manageuser.client_id', '=', 'client.client_id')
                             -> where('manageuser.client_id', $paramClientID, $whereClientID)
                             -> where('manageuser.username', $paramUsername, $whereUsername)
                             -> where('manageuser.memberis', $paramMemberIs, $whereMemberIs)
                             -> get();
        
        $data ['usermenu'] = DB::table('permission')
                             -> select('permission.menu_id as menu_id', 'manageuser_id', 'menu')
                             -> where('permission.client_id', $paramClientID, $whereClientID)
                             -> join('menu','menu.menu_id','=','permission.menu_id')
                             -> get();
        return response()->json($data);
    }

    public function notif(){
        if($this->checkSession()=='Y'){
            return view('managementNotif');
        }
        else{
            return redirect('/login');
        }
    }

    public function savemenu(Request $request)
    {
        if($this->checkSession()=='Y'){
            $vUsername = $request->username;
            $vMenu = $request->menu;

            $wordlist = DB::table('menu')->where('menu', '=', $vMenu)->get();
            $wordCount = $wordlist->count();

            if($wordCount > 0) {
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'Menu Already Exist '
                ]);
            }else{
                $vLogType = Session::get('datases')['logtype'];
                DB::table('permission')->insert([
                    'menu_id' => (collect(DB::select('select get_uuid() AS nb'))->first()->nb),
                    'menu' => $request->menu,
                    'link' => $request->link,
                    'status' => $request->status,
                    'parent_id' => $request->parent_id
                ]);

                return response()->json([
                    'type' => 'SUCCESS',
                    'message' => 'Menu Saved'
                ]);
            }
            
        }else{
            return redirect('/login');
        }
    }

    public function getparents()
    {
        $data = DB::table('menu')
                ->select('menu','menu_id')
                ->where('link','=','#')
                ->orWhereNull('link')
                ->get();

        return response()->json($data);
    }

    public function savewindow(Request $request)
    {
        if($this->checkSession()=='Y'){

            $wordlist = DB::table('menu')
                        ->where('menu', '=', $request->menu)
                        ->get();
            $wordCount = $wordlist->count();
            
            if($wordCount > 0) {
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'Menu Name Already Exist '
                ]);
            }

            $wordlist = DB::table('menu')
                        ->where('link', '=', $request->link)
                        ->where('link', '!=', '')
                        ->get();   
            $wordCount = $wordlist->count();

            if($wordCount > 0) {
                return response()->json([
                    'type' => 'ERROR',
                    'message' => 'Window With This Link Already Exist '
                ]);
            }
            DB::table('menu')->insert([
                'menu_id' => (collect(DB::select('select get_uuid() AS nb'))->first()->nb),
                'menu' => $request->menu,
                'link' => $request->link,
                'status' => $request->status,
                'parent_id' => $request->parent_id,
                'createdby' => $request->createdby,
                'created' => now()
            ]);

            return response()->json([
                'type' => 'SUCCESS',
                'message' => 'Window Saved'
            ]);
            
        }else{
            return redirect('/login');
        }
    }

    public function deletewindow(Request $request)
    {
        if ($this->checkSession() == 'Y') {

            $cWindow = count($request->menu_id);
            for ($i = 0; $i < $cWindow; $i++) {

                DB::table('menu')->Where('parent_id', $request->menu_id[$i])->delete();

                DB::table('menu')->where('menu_id', $request->menu_id[$i])->delete();
            }

            return response()->json([
                'type' => 'SUCCESS',
                'message' => 'Menu Window Deleted.'
            ]);
        } else {
            return redirect('/login');
        }
    }

    public function deletepermission(Request $request)
    {
        if ($this->checkSession() == 'Y') {

            $cWindow = count($request->menu_id);
            for ($i = 0; $i < $cWindow; $i++) {
                DB::table('permission')->where('menu_id', $request->menu_id[$i])->delete();

                $menuChild = DB::table('menu')
                            ->select('menu_id')
                            ->Where('parent_id', $request->menu_id[$i])->get();
        
                for ($i = 0; $i < count($menuChild); $i++) {
                    DB::table('permission')->where('menu_id', $menuChild[$i]->menu_id)->delete();
                }
            }

            return response()->json([
                'type' => 'SUCCESS',
                'message' => 'Menu Window Deleted.'
            ]);
        } else {
            return redirect('/login');
        }
    }

    public function getwindows()
    {
        $data = DB::table('menu as a')
              ->select('a.*', 'b.menu as parent_menu')
              ->leftjoin('menu as b', 'b.menu_id', '=', 'a.parent_id')
              ->get();
        
        return response()->json($data);
    }
}
