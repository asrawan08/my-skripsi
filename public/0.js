(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../index */ "./resources/js/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_index__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _cn_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../cn-api */ "./resources/js/cn-api.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_2__);
function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    disabled: Boolean,
    filled: Boolean,
    label: String,
    value: String,
    apiUrl: String,
    ruleList: Array,
    parentProviderId: String,
    initValueOpt: {
      type: Array
    }
  },
  data: function data() {
    return {
      dataId: this.value ? JSON.parse(JSON.stringify(this.value)) : "",
      dataIdTemp: "",
      searchDataOpt: "",
      dataOptPage: 1,
      dataOpt: [],
      dataTemp: [],
      dataOptLastPage: false,
      loading: false
    };
  },
  mounted: function mounted() {
    if (this.initValueOpt) {
      var _iterator = _createForOfIteratorHelper(this.initValueOpt),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var element = _step.value;

          if (typeof element.id === "string" && typeof element.name === "string") {
            var newVal = JSON.parse(JSON.stringify(this.initValueOpt));
            this.dataOpt = newVal;
            this.dataTemp = newVal;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
  },
  watch: {
    initValueOpt: function initValueOpt(val) {
      if (val) {
        var _iterator2 = _createForOfIteratorHelper(val),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var element = _step2.value;

            if (typeof element.id === "string" && typeof element.name === "string") {
              var newVal = JSON.parse(JSON.stringify(val));
              this.dataOpt = newVal;
              this.dataTemp = newVal;
            }
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
      }
    },
    parentProviderId: function parentProviderId(val) {
      if (!val) {
        if (this.dataOpt.length > 0) {
          this.dataOpt.splice(0, this.dataOpt.length);
        }
      }
    },
    searchDataOpt: function searchDataOpt(val) {
      if (val || val === "") {
        if (val === "") {
          this.dataId = "";
        }

        this.dataOptPage = 1;
        this.debouncedPosOpt({
          search: val,
          page: this.dataOptPage
        });
      } else {
        this.dataId = "";
      }
    },
    dataId: function dataId(val) {
      if (val) {
        var index = this.dataOpt.findIndex(function (item) {
          return item.id == val;
        });

        if (index > 0) {
          this.dataTemp.push(this.dataOpt[index]);
        } else {
          this.dataTemp = [];
        }
      } else {
        this.dataTemp = [];
      }

      this.$emit("input", val);
    },
    value: function value(val) {
      this.dataId = val;
    }
  },
  methods: {
    endIntersectPosOpt: function endIntersectPosOpt(entries, observer, isIntersecting) {
      if (isIntersecting && !this.dataOptLastPage && this.parentProviderId) {
        this.getDataOpt({
          page: this.dataOptPage,
          search: this.searchDataOpt,
          intersect: true
        });
      }
    },
    debouncedPosOpt: Object(_index__WEBPACK_IMPORTED_MODULE_0__["debounce"])(function (payload) {
      if (!this.disabled && this.parentProviderId) {
        this.getDataOpt(payload);
      }
    }, 500),
    getDataOptAtClick: function getDataOptAtClick() {
      this.dataOptPage = 1;
      this.dataOptLastPage = false;

      if (!this.disabled) {
        this.getDataOpt({});
      }
    },
    getDataOpt: function getDataOpt(payload) {
      this.loading = true;
      var me = this;
      _cn_api__WEBPACK_IMPORTED_MODULE_1__["default"].call({
        method: "GET",
        url: me.apiUrl + ("?pageNo=" + me.dataOptPage + "&pageSize=10") + (me.searchDataOpt ? "&term=" + me.searchDataOpt : "") + (me.parentProviderId ? "&provider-id=" + me.parentProviderId : ""),
        headers: {
          Authorization: "Bearer " + js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.get("token"),
          "Content-Type": "application/json",
          Accept: "application/json",
          "X-Client-ID": js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.get("clientId"),
          "X-User": js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.get("username")
        },
        ignoreLoading: true,
        onResult: function onResult(result) {
          me.loading = false;
          console.log("items", result);

          if (result.resultCode == 1 && result.items.length > 0) {
            var items = result.items;

            var _iterator3 = _createForOfIteratorHelper(me.dataTemp),
                _step3;

            try {
              var _loop = function _loop() {
                var element = _step3.value;
                var index = items.findIndex(function (item) {
                  return item.id == element.id;
                });
                if (index >= 0) items.splice(index, 1);
              };

              for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                _loop();
              }
            } catch (err) {
              _iterator3.e(err);
            } finally {
              _iterator3.f();
            }

            if (payload.intersect || me.dataOptPage == 1) {
              if (items.length >= 10) {
                me.dataOptPage++;
                me.dataOptLastPage = false;
              } else {
                me.dataOptLastPage = true;
              }

              me.dataOpt = me.dataOpt.concat(items);
            } else {
              me.dataOpt = me.dataTemp.concat(items);
              me.dataOptPage = 1;
            }
          } else {
            if (me.dataOpt.length > 0) {
              me.dataOpt.splice(0, me.dataOpt.length);
            }
          }
        }
      });
    },
    onChange: function onChange(val) {
      this.$emit("change", this.dataOpt.find(function (obj) {
        return obj.id === val;
      }).name);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=template&id=891c7c4e&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=template&id=891c7c4e& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "display-content-only" },
    [
      _c("v-autocomplete", {
        attrs: {
          loading: _vm.loading,
          items: _vm.dataOpt,
          "item-text": "name",
          "item-value": "id",
          disabled: _vm.disabled,
          filled: _vm.filled,
          label: _vm.label,
          dense: "",
          rules: _vm.ruleList ? _vm.ruleList : [],
          "search-input": _vm.searchDataOpt
        },
        on: {
          "update:searchInput": function($event) {
            _vm.searchDataOpt = $event
          },
          "update:search-input": function($event) {
            _vm.searchDataOpt = $event
          },
          change: _vm.onChange,
          click: _vm.getDataOptAtClick
        },
        scopedSlots: _vm._u([
          {
            key: "append-item",
            fn: function() {
              return [
                _c("div", {
                  directives: [
                    {
                      name: "intersect",
                      rawName: "v-intersect",
                      value: _vm.endIntersectPosOpt,
                      expression: "endIntersectPosOpt"
                    }
                  ]
                })
              ]
            },
            proxy: true
          }
        ]),
        model: {
          value: _vm.dataId,
          callback: function($$v) {
            _vm.dataId = $$v
          },
          expression: "dataId"
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VProviderVehicle_vue_vue_type_template_id_891c7c4e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VProviderVehicle.vue?vue&type=template&id=891c7c4e& */ "./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=template&id=891c7c4e&");
/* harmony import */ var _VProviderVehicle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VProviderVehicle.vue?vue&type=script&lang=js& */ "./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VProviderVehicle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VProviderVehicle_vue_vue_type_template_id_891c7c4e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VProviderVehicle_vue_vue_type_template_id_891c7c4e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VProviderVehicle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./VProviderVehicle.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VProviderVehicle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=template&id=891c7c4e&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=template&id=891c7c4e& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VProviderVehicle_vue_vue_type_template_id_891c7c4e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./VProviderVehicle.vue?vue&type=template&id=891c7c4e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/vehicle_gps_mapping/VProviderVehicle.vue?vue&type=template&id=891c7c4e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VProviderVehicle_vue_vue_type_template_id_891c7c4e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VProviderVehicle_vue_vue_type_template_id_891c7c4e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);