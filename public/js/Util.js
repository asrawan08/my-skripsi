/* Author: Hari Supriyanto
 * Created: Nov 2017
 * Updated: Feb 2018
 */

String.prototype.toCamelCase = function() {
    return this.replace(/^([A-Z])|[\s_](\w)/g, function(match, p1, p2, offset) {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();        
    });
};
var HariUtil = { 
	fillElementObjects: function(master, elementIds) {
		for (var key in elementIds) {
	    	var value = elementIds[key];
	    	master["$"+key] = $("#"+value);
	    	master["el_"+key] = value;
	    	//master["jq_"+key] = "#"+value;
	    };
	},
	Select2: (function() {
		var _private = function(model, isReadonly) {	
			var sobat = $("#"+model.ID);
			
			if (model.mode === "RECREATE") {
				sobat.select2('destroy').empty().select2(model);
			} else {
				sobat.select2(model);
			}
			if (isReadonly) {
				sobat.prop('disabled', isReadonly);
			}
			sobat.change(function() {				
				if (model.inputValueId) {
					$("#"+model.inputValueId).val($(this).val());
				}
				if (model.onChanged) {
					model.onChanged($(this).val()); 
				}
			});
			if (model.initSelect === "FIRST_ITEM" && model.data.length > 0) {
				sobat.val(model.data[0].id).trigger("change");
			} else if (model.initSelect === "BLANK" && model.allowClear) {
				sobat.val(null).trigger("change");
			} else if (model.initSelect) {
				sobat.val(model.initSelect.id || model.initSelect).trigger("change");
			}
			return sobat;
		};
		return {
			create : function(model, isReadonly) {
				console.log("### createselect2: ", model);
				model.mode = "NEW";
				return _private(model, isReadonly);
			},
			recreate : function(model, isReadonly) {
				console.log("### recreateselect2: ", model);
				model.mode = "RECREATE";
				return _private(model, isReadonly);
			}
		};
	}()),
	arrayToObject: function(arr) {
		var json = arr.reduce(function(o, val) { 
			o[val.id] = val; 
			return o; 
		}, {});
		return json;
	},
	objectToArray: function(obj) {
		var arr = [];
		if (obj) {
			for (var key in obj) {
				arr.push(obj[key]);
			}
		}
		return arr;
	},
	leadingZero: function(num, size) {
		var s = String(num);
		while (s.length < (size || 2)) {s = "0" + s;}
		return s;
	},
	addDays: function(date, days) {
		var dt = date || new Date();
		dt.setDate(new Date().getDate()+days);
		return dt;
	},
	formatIdTime: function (dataTime){
		if(dataTime){
			var date = dataTime.split(/T|\s/);
			var aarDate = date[0];
			var arrTime = date[1];
			var getDate = aarDate.split("-")
			var getTime = arrTime.split(".")
			var getTimeReal = getTime[0].split(":")
			return getDate[2]+"/"+getDate[1]+"/"+getDate[0]+" "+getTimeReal[0]+":"+getTimeReal[1]+":"+getTimeReal[2];
		} 
		return "anjing";
	},
	formatDate: function(date, format, appender) {
		if (format == undefined) {
			format = "DATE";
		}
		if (date == undefined) {
			date = new Date();
		}
		if (appender == undefined) {
			appender = "";
		}
		var formattedDate = "";
		if (format == "DATE" || format == "STDDATE" || format == "DATESET" || format == "DATETRIM" ) {
			var d = this.leadingZero(date.getDate(), 2);
			var m = this.leadingZero(date.getMonth() + 1, 2);
			var y = date.getFullYear();
			if (format == "DATETRIM") {
				formattedDate = y + m + d;
			} else if (format == "STDDATE") {
				formattedDate = y + "-" + m + "-" + d;
			} else if (format == "DATESET") {
				formattedDate = y + "," + m + "," + d;
			}else {
				formattedDate = d + "/" + m + "/" + y;
			}
			formattedDate = formattedDate + appender;
		} else if (format == "DATETIME" || format == "DATETIMETRIM") {
			var h = this.leadingZero(date.getHours(), 2);
			var min = this.leadingZero(date.getMinutes(), 2);
			
			if (format == "DATETIMETRIM") {
				formattedDate = this.formatDate(date, "DATETRIM", h+min);
			} else {
				formattedDate = this.formatDate(date, "DATE", " "+h+":"+min);
			}
			formattedDate = formattedDate + appender;
		} else if (format == "DATETIMEMS" || format == "DATETIMEMSTRIM") {
			var s = this.leadingZero(date.getSeconds(), 2);
			
			if (format == "DATETIMEMSTRIM") {
				formattedDate = this.formatDate(date, "DATETIMETRIM", s);
			} else {
				formattedDate = this.formatDate(date, "DATETIME", ":"+s);
			}
			formattedDate = formattedDate + appender;
		}
		return formattedDate;
	},
	getBaseUrl: function() {
		var protocol = window.location.protocol;
		var host = window.location.host;
		var port = window.location.port==='80' || window.location.port==='443' ? '' : (':'+window.location.port);
		var pathArr = window.location.pathname.split('/');

		return protocol+'//'+host+'/'+pathArr[1];
	},
	loadingStart: function(popup) {
		if (popup && popup.freeze) {
			popup.freeze();
		}
		$('body').loadingModal({
		  	text: 'Loading...',
		  	opacity: '0.2'
		});
	},
	loadingFinish: function(popup) {
		if (popup && popup.unfreeze) {
			popup.unfreeze();
		}
		$('body').loadingModal('destroy');
	},
	replaceAll: function(str, search, replacer) {
		return str.split(search).join(replacer);
	},
	getLastUpdate: function(miliseconds) {
		if (miliseconds > (1000*60*60)) {
			lastUpdate = (miliseconds/60/60/1000).toFixed(2)+" jam yang lalu";
		} else if (miliseconds > (1000*60)) {
			lastUpdate = (miliseconds/60/1000).toFixed(2)+" menit yang lalu";
		} else if (miliseconds > 1000) {
			lastUpdate = (miliseconds/1000).toFixed(2)+" detik yang lalu";
		} else {
			lastUpdate = "1 detik yang lalu";
		} 
		return lastUpdate;
	},
	getVAUCommonName: function(vauName) {
		var names = vauName.split(' - ');
		if (names.length == 1) {
			return vauName;
		} 
    	var brand = names[0];
    	var commonNames = brand;
    	for (var i=1; i<names.length; i++) {
    		commonNames = commonNames + ' ' +names[i].replace(brand, '').trim();
    	}
    	return commonNames;
	},
	formatPlateNo: function(plateNo) {
		if (plateNo == undefined)
			return "";
		return plateNo.replace(/(\w)\s*(\d+)\s*(\w+)/,"$1 $2 $3");
	},
	findInJSONArray: function (array, key, value) {
	    return array.filter(function (object) {
	        return object[key] === value;
	    });
	},
	createSelect2: function(cfg, varsToSend) {
		var me = this;
		var dataitems;
		var $sel2 = $("#"+cfg.id).select2({
		    placeholder: cfg.title,
		    minimumInputLength: 0,
	       	allowClear: cfg.allowClear ? cfg.allowClear==="Y" : true,
		    ajax: !cfg.actionUrl ? null : {
		    	url: cfg.actionUrl,
		    	type: 'POST',
		        dataType: 'json',	        
		       	closeOnSelect: false,
		        contentType: "application/json;charset=UTF-8",
		        quietMillis: 500,
		        headers: {
        			"Authorization" : "Bearer "+cfg.accessToken,
					"Accept": "application/json",
        			"Content-Type" : "application/json",
    			},
		        error: function (jqXHR, status, error) {
		        	console.log("AJAX ERROR status: ", status);
		        	console.log("AJAX ERROR error: ", error);
		            console.log("Object: ",JSON.stringify(jqXHR));
		            return { results: [] }; 
		        },
		        data: function (scrolldata) {
					console.log("SCROLLDATA = ",scrolldata);
		        	var postdata = {};
		        	if (cfg.paging) {
		        		postdata.term = scrolldata.term;		        	
		        		postdata.page = scrolldata.page;
		        		postdata.pageSize = 6;
		        	}
		        	if (cfg.lazyVars) {
		        		for (var i=0; i<cfg.lazyVars.length; i++) {
		        			var lazyvar = cfg.lazyVars[i];
		        			postdata[lazyvar.name] = lazyvar.fnGet();
		        		}
		        	}
		        	if (varsToSend) {
		        		for (var key in varsToSend) {
		        			postdata[key] = varsToSend[key];
		        		}
		        	}
		        	return JSON.stringify(postdata);
		        },
		        processResults: function (data, term) {
					console.log("ProcessResults: ",data);
		        	dataitems = data.items;
		        	if (cfg.paging) {
			            var more = (data.page * data.countItems) < data.totalItems;
			            return { results: data.items, pagination: {more: more}};
		        	} else {
		        		return { results: data.items };
		        	}
		        }
		    }
		});
		if (cfg.onChange) {
			$sel2.on("change", cfg.onChange);
		}
		return $sel2;
	}
}
var EmailUtil = {
	collectSelectedEntity: function(srcEntities, selectedAddressArr) {
		var result = [];		
		for (var i=0; i<selectedAddressArr.length; i++) {
	    	var obj = HariUtil.findInJSONArray(srcEntities, "email", selectedAddressArr[i]);
	    	if (obj && obj[0]) {
	    		result.push(obj[0]);
	    	} else {
	    		var name = selectedAddressArr[i].replace(/@.*/,'');
	    		result.push({name: name, email: selectedAddressArr[i]});
	    	}
	    }
		return result;
	},
	createSelectize: function(inputEl, contacts, maxItems) {
		var REGEX_EMAIL = 	'([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
		 					'(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';
		return $(inputEl).selectize({
		    persist: false,
		    dropdownParent: "body",
		    maxItems: maxItems!=undefined && maxItems!=null ? maxItems : null,
		    valueField: 'email',
		    labelField: 'name',
		    searchField: ['name', 'email'],
		    inputClass: 'selectize-input',
		    options: contacts || [],
		    render: {
		        item: function(item, escape) {
		            return '<div>' +
		                (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
		                (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
		            '</div>';
		        },
		        option: function(item, escape) {
		            var label = item.name || item.email;
		            var caption = item.name ? item.email : null;
		            return '<div>' +
		                '<span class="selectize-label">' + escape(label) + '</span>' +
		                (caption ? '<span class="selectize-caption">' + escape(caption) + '</span>' : '') +
		            '</div>';
		        }
		    },
		    createFilter: function(input) {
		        var match, regex;

		        // email@address.com
		        regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
		        match = input.match(regex);
		        if (match) return !this.options.hasOwnProperty(match[0]);

		        // name <email@address.com>
		        regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
		        match = input.match(regex);
		        if (match) return !this.options.hasOwnProperty(match[2]);

		        return false;
		    },
		    create: function(input) {
		        if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
		            return {email: input};
		        }
		        var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
		        if (match) {
		            return {
		                email : match[2],
		                name  : $.trim(match[1])
		            };
		        }
		        alert('Invalid email address.');
		        return false;
		    }
		});
	}
};
var Info = function() {
	this.text = "";
	this.isEmpty = true;
	this.addTitle = function(value) {
		this.text += "<strong>"+value+"</strong>";
		this.isEmpty = false;
	};
	this.addLine = function(label, value) {
		if (this.text.length>0) {
			this.text += "<br>";	
		}
		if (value === undefined || value === null) {
			value = label;
		} else {
			this.text += "<strong>"+label+":  </strong>";
		}		
		this.text += value;
		this.isEmpty = false;
	};
	this.append = function(value) {
		this.text += value;
	};
	this.clear = function() {
		this.text = null;
	};
	this.getText = function() {
		return this.text;
	};
};