/* Author: Hari Supriyanto
 * Created: Des 2019
 * Updated: -
 */
 
var APIUtil = {
	callAPI: function(apiOptions) {
	  console.log("Call API. postdata: ",apiOptions.postData);
	  $.ajax({
	    url: apiOptions.url,
	    type: apiOptions.method||"GET",
 	    headers: {
	        "Content-Type":"application/json",
	        "Accept": "application/json",
	        "Authorization":"Bearer "+apiOptions.accessToken
	    },
	    dataType: "JSON",
	    accept: "application/json",
	    contentType:"application/json",
	    data: !apiOptions.method || apiOptions.method==="GET" ? null : JSON.stringify(apiOptions.postData||"{}"),
	    success: function(response){
			console.log(document.cookie['token']);
	      console.log('response',response);
	      if (typeof apiOptions.onSuccess === 'function') {
	        apiOptions.onSuccess(response);   
	      } else if (typeof apiOptions.onResultItems === 'function') {
	        if (response.resultCode>0 && response.items) {
	          apiOptions.onResultItems(response.items, response.pageNo||apiOptions.postData.pageNo, response.totalItems);
	        }
	      }     
	      return;
	    },
	    error: function (jqXHR, status, error) {
	      if (typeof apiOptions.onError === 'function') {
	        apiOptions.onError(jqXHR, status, error);
	      } else {
	        console.log("AJAX ERROR status: ", status);
	        console.log("AJAX ERROR error: ", error);
	        console.log("Object: ",JSON.stringify(jqXHR));
	        return { results: [] }; 
	      }
	    }
	  });
	}
};